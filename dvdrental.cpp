#include <iostream>
#include <string.h>
#include <ctime>
#include <windows.h>

using namespace std;


int main()
{ 

	string movietitle[] = 
	{
		"0",
		"Divergent",
		"Midnight Sun",
		"Love,Rosie",
		"3 Idiots",
		"Percy Jackson and the Lightning Thief",
		"Train to Busan",
		"The Karate Kid",
		"Crazy Little Thing Called Love",
		"Now You See Me 2",
		"Captain America the First Avenger",
		"Captain America: The Winter Soldier",
		"Captain America:Civil War",
		"The Purge: Election Year",
		"The Purge",
		"The Purge: Anarchy",
		"The First Purge",
		"Gifted",
		"The Chronicles of Narnia: The Lion, the Witch and the Wardrobe",
		"The Chronicles of Narnia: Prince Caspian",
		"The Chronicles of Narnia: The Voyage of the Dawn Treader",
		"Harry Potter and the Prisoner of Azkaban",
		"Harry Potter and the Order of the Phoenix",
		"Harry Potter and the Half-Blood Prince",
		"Hachi: A Dog's Tale",
		"The Princess Diaries"
		};
	
	string director[] = 
	{
		"0",
		"Neil Burger",
		"Scott Speer",
		"Christian Ditter",
		"Rajkumar Hirani",
		"Chris Columbus",
		"Yeon Sang-ho",
		"Harald Zwart",
		"Puttipong Pormsaka Na-Sakonnakorn and Wasin Pokpong",
		"Jon M. Chu",
		"Joe Johnston",
		"Anthony Russo and Joe Russo",
		"Anthony Russo, Joe Russo",
		"James DeMonaco",
		"James DeMonaco",
		"James DeMonaco",
		"Gerard McMurray",
		"Marc Webb",
		"Andrew Adamson",
		"Andrew Adamson",
		"Michael Apted",
		"Alfonso Cuaron",
		"David Yates",
		"David Yates",
		"Lasse Hallstrom",
		"Garry Marshall"
		};

	string producers[] = 
	{
		"0",
		"Lucy Fisher, Pouya Shahbazian",
		"Jen Gatien, John Rickard, and Zack Schiller",
		"Robert Kulzer, Simon Brooks",
		"Vidhu Vinod Chopra",
		"Karen Rosenfelt, Chris Columbus, Michael Barnathan, and Mark Radcliffe",
		"Lee Dong-ha",
		"Jerry Weintraub, Will Smith, Jada Pinkett Smith",
		"Somsak Tejcharattanaprasert and Panya Nirankol",
		"Alex Kurtzman, Roberto Orci, Bobby Cohen",
		"Kevin Feige",
		"Kevin Feige",
		"Kevin Feige",
		"Jason Blum, Michael Bay, Andrew Form",
		"Jason Blum, Michael Bay, Andrew Form",
		"Jason Blum, Michael Bay, Andrew Form",
		"Jason Blum, Michael Bay, Andrew Form",
		"Karen Lunder, Andy Cohen",
		"Mark Johnson, Phillip Steuer",
		"Mark Johnson, Andrew Adamson, Philip Steuer",
		"Mark Johnson, Andrew Adamson, Philip Steuer" ,
		"Chris Columbus, David Heyman, Mark Radcliffe",
		"David Heyman and David Barron",
		"David Heyman and David Barron",
		"Richard Gere",
		"Whitney Houston, Debra Martin Chase"
		};
	
	string actors[] = 
	{
		"0",
		"Shailene Woodley, Theo James",
		"Bella Thorne, Patrick Schwarzenegger",
		"Lily Collins, Sam Claflin",
		"Aamir Khan, R. Madhavan, Sharman Joshi",
		"Logan Lerman, Brandon T. Jackson, Alexandra Daddario",
		"Gong Yoo, Ma Dong-seok,Jung Yu-mi",
		"Jaden Smith, Jackie Chan, Taraji P. Henson",
		"Mario Maurer, Pimchanok Luevisadpaibul",
		"Jesse Eisenberg, Mark Ruffalo, Woody Harrelson, Dave Franco",
		"Chris Evans, Tommy Lee Jones, Hugo Weaving, Hayley Atwell",
		"Chris Evans, Scarlett Johansson, Sebastian Stan, Anthony Mackie",
		"Chris Evans, Robert Downey Jr., Scarlett Johansson, Sebastian Stan",
		"Frank Grillo, Elizabeth Mitchell, Mykelti Williamson",
		"Ethan Hawke, Lena Headey, Adelaide Kane, Max Burkholder",
		"Frank Grill, Carmen Ejogo, Zach Gilford, Kiele Sanche",
		"Y'lan Noel, Lex Scott Davis, Joivan Wade, Steve Harris",
		"Chris Evans, Mckenna Grace, Lindsay Duncan, Jenny Slate",
		"William Moseley, Anna Popplewell, Skandar Keynes, Georgie Henley",
		"William Moseley, Anna Popplewell, Skandar Keynes, Georgie Henley, Ben Barnes",
		"Georgie Henley, Skandar Keynes, Will Poulter, Ben Barnes",
		"Daniel Radcliffe, Rupert Grint, Emma Watson", 
		"Daniel Radcliffe, Rupert Grint, Emma Watson",
		"Daniel Radcliffe, Rupert Grint, Emma Watson",
		"Richard Gere, Joan Allen",
		"Anne Hathaway, Julie Andrews, Heather Matarazzo"
		};


	string dvdavailability[] = {"0","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  "};
	string customers[41];
	string renteddvd[41];
	string dateofrent[41];
	string customername;
	int chosendvd;
	int numberchosen;
	int dvdnumberchosen;
	
	
	
	bool num = true;
	int ber=0;

	while(num = true)
	{
		system("CLS");
    	cout<< "\t\t\t\tDVD RENTAL SHOP\t\t\t\t" << endl;
		cout << "\n\nChoose from the following: " << endl
			 << "1 : DVD Availability" << endl
			 << "2 : Rent/Check-out DVD" << endl 
			 << "3 : Return DVD" << endl 
			 << "4 : View Customers" << endl 
			 << "5 : exit" << endl
			
			<< "\n\nEnter the number you want to choose: ";

			
		cin >> dvdnumberchosen;
		if(cin.fail())

		{ 
			cin.clear();
			cin.ignore();
			system("CLS");
			cout << "INVALID INPUT" << endl;
			system("PAUSE");
			system("CLS");
		}

		if (dvdnumberchosen == 1)
		{	
			system("CLS");
			cout << "DVD LIST"  << endl <<endl;

			cout << "AVAILABILITY:               TITLE OF THE MOVIE:" << endl;

			for(int x1=1;x1<=25;x1++)
				{
					cout << dvdavailability[x1] << "            " << x1 << ": "<< movietitle[x1] << endl;
				}

			cout << "END OF LIST" << endl << endl;
			cout << "Choose number to view: ";
			cin >> chosendvd;

			if(cin.fail() or chosendvd <= 0 or chosendvd >=26)

			{
				cin.clear();

	     		cin.ignore();

				cout << "INVALID INPUT" << endl;

				system("PAUSE");

				system("CLS");

			}

			else

			{

				system("CLS");

				cout << "TITLE: " << movietitle[chosendvd] << endl 

					 << "STATUS: " << dvdavailability[chosendvd] << endl 

					 << "DIRECTOR: " << director[chosendvd] << endl 

					 << "ACTORS: " << actors[chosendvd] << endl

					 << "Producers: " << producers[chosendvd] << endl 

					 << endl;

				system("PAUSE");

			}

		}

		


		
		else if (dvdnumberchosen == 2)

		{

			system("CLS");
			cout << "DVD LIST"  << endl <<endl;

			cout << "STATUS:                TITLE:" << endl;

			for(int x1=1;x1<=25;x1++)

				{

					cout << dvdavailability[x1] << "            " << x1 << ": "<< movietitle[x1] << endl;

				}

			cout << "END OF LIST" << endl << endl;

					bool r = true;

					while(r == true)

					{

						cout << "To rent a dvd select its number: ";

						cin >> chosendvd;

						if(cin.fail() or chosendvd <= 0 or chosendvd >=26)

						{

							cout << "INVALID INPUT" << endl;

							cin.clear();

							cin.ignore();

							r = true;

						}

						else

						{

							r = false;

						}

					}

					if (dvdavailability[chosendvd] == "UNAVAILABLE")

					{

						cout << "THE DVD YOU CHOSE IS UNAVAILABLE" << endl;

						system("PAUSE");

					}

					else

					{

						bool p = true;

						while(p == true)

						{

							cout << "Enter your name: ";

							cin >> customers[ber];

							if(cin.fail())

							{

								cin.clear();

								p = true;

							}

							else

							{

								p = false;

							}

						}

						renteddvd[ber] = movietitle[chosendvd];


						time_t curr_time;

						tm * curr_tm;

						char date_string[100];

						time(&curr_time);

						curr_tm = localtime(&curr_time);

						strftime(date_string, 50, "%B %d, %Y", curr_tm);

						dateofrent[ber] = date_string;

						cout << "RECEIPT" << endl;

						cout << "CUSTOMER NAME: " << customers[ber] << endl;

						cout << "DVD RENTED: " << renteddvd[ber] << endl;

						cout << "DATE RENTED: " << dateofrent[ber] << endl;

						system("PAUSE");

						ber=++ber;

						dvdavailability[chosendvd] = "UNAVAILABLE";

					}

		}

				else if (dvdnumberchosen == 3)

		{
			system("CLS");

			cout << "RETURN_IN" << endl <<endl;

			cout << "STATUS:                #: TITLE:" << endl;

			for(int x1=1;x1<=25;x1++)

				{

					cout << dvdavailability[x1] << "            " << x1 << ": "<< movietitle[x1] << endl;

				}

			cout << "END OF LIST" << endl << endl;

					bool r = true;

					while(r == true)

					{

						cout << "To return a DVD chose its number: ";

						cin >> chosendvd;

						if(cin.fail() or chosendvd <= 0 or chosendvd >=26)

						{

							

							cout << "INVALID INPUT" << endl;

							cin.clear();

							cin.ignore();

							r = true;

						}

						else

						{

							r = false;

						}

					}

					if (dvdavailability[chosendvd] == "AVAILABLE  ")

					{

						cout << "You cannot yet return it." << endl;
						system("PAUSE");

					}

					else

					{
						dvdavailability[chosendvd] = "AVAILABLE  ";
						cout << "You successfully return it." << endl;
						system("PAUSE");

					}

			

		}

		else if (dvdnumberchosen == 4)

		{
			system("CLS");

			cout << "RECORDS OF RENT "<< endl;

			for(int xx= 0;xx<=ber;xx++)

			{

				if (customers[xx] != "")

				{

					cout<< " " << endl

						<< "CUSTOMER NAME: " << customers[xx] << endl

						<< "DVD RENTED: " << renteddvd[xx] << endl

						<< "DATE RENTED: " << dateofrent[xx] << endl		

						<< "  " << endl;

				}	

			}

			system("PAUSE");

		}

		else if (dvdnumberchosen == 5)

		{
			exit(0);

		}

		else

		{

			cout << "INVALID INPUT" << endl;

			system("PAUSE");

		}

	}

}